<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Profile;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected function redirectTo()
    {
        if (auth()->check()) {
            if (auth()->user()->role_user == 'admin') {
                return '/admin';
            } else {
                return '/';
            }
        } else {
            return '/login';
        }
    }

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'umur' => ['required'],
            'alamat' => ['required'],
        ]);
    }

    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'role_user' => 'user', // Menambahkan kolom role_user dan set value ke 'user'
        ]);

        Profile::create([
            'umur' => $data['umur'],
            'alamat' => $data['alamat'],
            'users_id' => $user->id
        ]);
        return $user;
    }

    protected function registered(Request $request, $user)
    {
        return redirect('/login')->with('success', 'Akun berhasil dibuat! Silakan login untuk melanjutkan.');
    }
}

<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function create(){
        return view('admin.category.create');
    }

    public function store(Request $request){
        $request -> validate([
            'category_name' => 'required',
        ]);
    
        DB::table('category')->insert([
            'category_name' => $request ['category_name'],
        ]);
    
        return redirect('/admin/category');
    }
    

    public function index(){
        $category = DB::table('category')->get();
 
        return view('admin.category.index', ['category' => $category]);
    }

    public function edit($id){
        $category = DB::table('category')->find($id);

        return view('admin.category.edit', ['category' => $category]);
    }

    public function update(Request $request, $id){
        $request -> validate([
            'category_name' => 'required',
        ]);

        DB::table('category')
        ->where('id', $id)
        ->update([
            'category_name' => $request ['category_name'],
        ]);

        return redirect('/admin/category');
    }

    public function destroy($id){
        $deleted = DB::table('category')->where('id', '=', $id)->delete();

        return redirect('/admin/category');
    }
}

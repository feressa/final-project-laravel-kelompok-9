<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class KeranjangController extends Controller
{
    public function create(){
        return view('admin.keranjang.create');
    }

    public function store(Request $request){
        $request -> validate([
            'keranjang_name' => 'required',
        ]);
    
        DB::table('keranjang')->insert([
            'keranjang_name' => $request ['keranjang_name'],
        ]);
    
        return redirect('/admin/keranjang');
    }
    

    public function index(){
        $keranjang = DB::table('keranjang')->get();
 
        return view('admin.keranjang.index', ['keranjang' => $keranjang]);
    }

    public function edit($id){
        $keranjang = DB::table('keranjang')->find($id);

        return view('admin.keranjang.edit', ['keranjang' => $keranjang]);
    }

    public function update(Request $request, $id){
        $request -> validate([
            'keranjang_name' => 'required',
        ]);

        DB::table('keranjang')
        ->where('id', $id)
        ->update([
            'keranjang_name' => $request ['keranjang_name'],
        ]);

        return redirect('/admin/keranjang');
    }

    public function destroy($id){
        $deleted = DB::table('keranjang')->where('id', '=', $id)->delete();

        return redirect('/admin/keranjang');
    }
}

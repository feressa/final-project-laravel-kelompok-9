<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Produk;
use Illuminate\Support\Facades\File;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = Produk::with('category')->get();
        return view('admin.produk.index', ['produk' => $produk]);

    }

    public function welcome()
    {
        $produk = Produk::with('category')->get();
        return view('welcome', ['produk' => $produk]);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::get();
        return view('admin.produk.create', ['categories' => $categories]);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'produk_name' => 'required',
            'description' => 'required',
            'gambar' => 'required|image|mimes:jpg,png,jpeg',
            'harga' => 'required',
            'stok' => 'required',
            'category_id' => 'required',
        ]);

        // Proses menyimpan data ke database
        $produk = new Produk;
        $produk->produk_name = $request->produk_name;
        $produk->description = $request->description;

        $fileName = time() . '.' . $request->gambar->extension();

        $request->gambar->move(public_path('uploads'), $fileName);

        $produk->harga = $request->harga;
        $produk->stok = $request->stok;
        $produk->category_id = $request->category_id;
        $produk->gambar = $fileName;

        $produk->save();

        return redirect('/admin');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Produk::find($id);
        $category = Category::get();

        return view('admin.produk.update', ['produk' => $produk, 'category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'produk_name' => 'required',
            'description' => 'required',
            'gambar' => 'image|mimes:jpg,png,jpeg',
            'harga' => 'required',
            'stok' => 'required',
            'category_id' => 'required',
        ]);

        $produk = Produk::find($id);


        if ($request->has('gambar')) {
            $path = public_path('uploads/' . $produk->gambar);
            File::delete($path);
            $fileName = time() . '.' . $request->gambar->extension();

            $request->gambar->move(public_path('uploads'), $fileName);

            $produk->gambar = $fileName;

            $produk->save();
        }

        $produk->produk_name = $request['produk_name'];
        $produk->description = $request['description'];
        $produk->harga = $request['harga'];
        $produk->stok = $request['stok'];
        $produk->category_id = $request['category_id'];
        $produk->save();

        return redirect('/admin/produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = Produk::find($id);

        $path = public_path('uploads/' . $produk->gambar);
        File::delete($path);

        $produk->delete();

        return redirect('/admin/produk');
    }
}

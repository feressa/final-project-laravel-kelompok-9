<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;

    protected $table = 'produk';
    protected $fillable = ['produk_name', 'description', 'gambar', 'harga', 'stok', 'category_id'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}


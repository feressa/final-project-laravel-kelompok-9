<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keranjang', function (Blueprint $table) {
            $table->id();
            $table->integer('jumlah_produk');
            $table->integer('harga_total');
            $table->enum('status', ['belum dibayar', 'dalam proses', 'selesai'])->default('belum dibayar');

            $table->unsignedBigInteger('produk_id');
 
            $table->foreign('produk_id')->references('id')->on('produk');
            $table->unsignedBigInteger('users_id');
 
            $table->foreign('users_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keranjang');
    }
};

@extends('layouts.admin')
@section('title')
    Tambah Data Category
@endsection

@section('content')
    <div class="card">
       
        <!-- /.card-header -->
        <div class="card-body">
            <form action="/admin/category" method="POST">
                @csrf
                <div class="form-group">
                    <label>Nama Category</label>
                    <input type="text" name="category_name" class="form-control">
                    <small class="form-text text-muted">Masukan Nama Kategori Produk Anda</small>
                </div>
                @error('category_name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <div class="flex">
                    <button type="submit" class="btn btn-primary">Submit</button> 
                    <a href="/admin/category/" class="btn btn-primary btn-md mt-2 mb-2">Kembali</a>
                </div>
                
            </form>
           
        </div>
        <!-- /.card-body -->
@endsection

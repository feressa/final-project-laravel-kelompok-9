@extends('layouts.admin')
@section('title')
    Tambah Data Category
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Tambah Data</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form action="/admin/category/{{ $category->id }}" method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Nama Category</label>
                    <input type="text" name="category_name" value="{{ $category->category_name }}" class="form-control">
                    <small class="form-text text-muted">Masukan Nama Kategori Produk Anda</small>
                </div>
                @error('category_name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <div class="flex">
                    <button type="submit" class="btn btn-primary">Submit</button> 
                    <a href="/admin/category/" class="btn btn-primary btn-md mt-2 mb-2">Kembali</a>
                </div>
                
            </form>
           
        </div>
        <!-- /.card-body -->
@endsection

@extends('layouts.admin')
@section('title')
    Data Kategori
@endsection

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Category</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <a href="/admin/category/create" class="btn btn-primary btn-md p-2 mb-2">Tambah Data Category</a>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Kategori</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($category as $key=> $category)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $category->category_name }}</td>
                                <td>
                                    <form action="/admin/category/{{ $category->id }}" method="post">
                                        <a href="/admin/category/{{ $category->id }}/edit" class="btn btn-warning btn-sm">Edit</a>

                                        @csrf
                                        @method('delete')
                                        <input type="submit" value="delete" c class="btn btn-danger btn-sm">
                                    </form>
                                </td>
                            </tr>
                        @empty
                        @endforelse



                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection

@extends('layouts.admin')
@section('title')
    Tambah Data Produk
@endsection

@section('content')
    <div class="card">
        
        <!-- /.card-header -->
        <div class="card-body">
            <form action="/admin/produk" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Nama Produk</label>
                    <input type="text" name="produk_name" class="form-control">
                    <small class="form-text text-muted">Masukan Nama Kategori Produk Anda</small>
                </div>
                @error('produk_name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Deskripsi</label>
                    <input type="text" name="description" class="form-control">
                    <small class="form-text text-muted">Masukan Deskripsi Produk Anda</small>
                </div>
                @error('description')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Gambar Produk</label>
                    <input type="file" name="gambar" class="form-control">
                    <small class="form-text text-muted">Masukan Gambar Produk Anda</small>
                </div>
                @error('gambar')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Harga</label>
                    <input type="text" name="harga" class="form-control">
                    <small class="form-text text-muted">Masukan Harga Produk Anda</small>
                </div>
                @error('harga')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Stok</label>
                    <input type="number" name="stok" class="form-control">
                    <small class="form-text text-muted">Masukan Stok Produk Anda</small>
                </div>
                @error('stok')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Kategori</label>
                    <select name="category_id" class="form-control">
                        <option value="">--Pilih Kategori--</option>
                        @forelse ($categories as $item)
                            <option value="{{ $item->id }}">{{ $item->category_name }}
                        @empty
                            <option value="">Tidak ada data Kategory</option>
                        @endforelse
                    </select>
                </div>
                @error('category_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <div class="flex">
                    <button type="submit" class="btn btn-primary">Submit</button> 
                    <a href="/admin" class="btn btn-primary btn-md mt-2 mb-2">Kembali</a>
                </div>
                
            </form>
           
        </div>
        <!-- /.card-body -->
@endsection

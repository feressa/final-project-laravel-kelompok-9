@extends('layouts.admin')
@section('title')
    Data Produk
@endsection

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <a href="/admin/produk/create" class="btn btn-primary btn-md p-2 mb-2">Tambah Data</a>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Produk</th>
                            <th>Deskripsi</th>
                            <th>Gambar</th>
                            <th>Harga Produk</th>
                            <th>Stok</th>
                            <th>Kategori</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        @forelse ($produk as $key => $item)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $item->produk_name }}</td>
                                <td>{{ $item->description }}</td>
                                <td><img src="{{ asset('uploads/' . $item->gambar) }}" style="height: 100px; width: 100px;"></td> 
                                <td>{{ $item->harga }}</td>
                                <td>{{ $item->stok }}</td>
                                <td>{{ $item->category->category_name }}</td>
                                <td>
                                    <form action="/admin/produk/{{ $item->id }}" method="post">
                                        <a href="/admin/produk/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>

                                        @csrf
                                        @method('delete')
                                        <input type="submit" value="delete" c class="btn btn-danger btn-sm">
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <h2>Data Kosong</h2>
                        @endforelse





                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection

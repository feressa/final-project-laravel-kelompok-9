<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\KeranjangController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// sudah bikin branch feressa

Route::get('/', function () {
    return view('welcome');
});

Route::get('/master', function(){
    return view('layouts.master');
});

Route::get('/menu', function(){
    return view('layouts.menu');
});

Route::get('/about', function(){
    return view('layouts.about');
});

Route::get('/book', function(){
    return view('layouts.book');
});

Route::get('/order', function(){
    return view('page.order-online');
});

//route admin

Route::get('/admin', function () {
    return view('admin.index');
});
Route::get('/admin/produk', function () {
    return view('admin.index');
});
Route::get('/admin/category', function () {
    return view('admin.category.index');
});

//CRUD CATEGORY
Route::get('/admin/category/create', [CategoryController::class, 'create']);
Route::post('/admin/category',[CategoryController::class, 'store']);
Route::get('/admin/category',[CategoryController::class, 'index']);
Route::get('/admin/category/{id}/edit',[CategoryController::class, 'edit']);
Route::put('/admin/category/{id}',[CategoryController::class, 'update']);
Route::delete('/admin/category/{id}',[CategoryController::class, 'destroy']);

//CRUD PRODUK
Route::resource('/admin/produk', ProdukController::class);

Route::get('/', [ProdukController::class, 'welcome']);

Auth::routes();

Route::get('/login', 'App\Http\Controllers\Auth\LoginController@showLoginForm')->name('login');
Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/admin', [AdminController::class, 'index']);
});
